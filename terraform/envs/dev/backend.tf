terraform {
  backend "s3" {
    bucket = "awsbuck12345"
    key    = "terraform/dev/"
    region = "us-east-1"
  }
}
